﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Diagnostics;
//86, 209

namespace Client
{
    public partial class Client : Form
    {

        private Socket _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        Thread t1;
        byte[] receivedBuf = new byte[1716964]; //1716964 450560

        const int WM_DEVICECHANGE = 0x0219;
        const int DBT_DEVICEARRIVAL = 0x8000;
        const int DBT_DEVICEREMOVALCOMPLETE = 0x8004;
        const int DBT_DEVTYPVOLUME = 0x00000002;

        bool sidebarExpand;

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
             int nLeftRect,     // x-coordinate of upper-left corner
             int nTopRect,      // y-coordinate of upper-left corner
             int nRightRect,    // x-coordinate of lower-right corner
             int nBottomRect,   // y-coordinate of lower-right corner
             int nWidthEllipse, // height of ellipse
             int nHeightEllipse // width of ellipse
        );

        public Client()
        {
            InitializeComponent();

            Rounded_TextBox(chatBox, 40, 250);
            Rounded_TextBox(txtName, 30, 180);
            Rounded_TextBox(txtRollno, 30, 180);

            MsgBox.BorderStyle = BorderStyle.None;
            MsgBox.AutoSize = false;
            MsgBox.Height = 410;
            MsgBox.Width = 301;
            MsgBox.Region = Region.FromHrgn(CreateRoundRectRgn(0, 0, MsgBox.Width, MsgBox.Height, 20, 20));

            panel259.BorderStyle = BorderStyle.None;
            panel259.AutoSize = false;
            panel259.Height = 400;
            panel259.Width = 270;
            panel259.Region = Region.FromHrgn(CreateRoundRectRgn(10, 10, panel259.Width, panel259.Height, 20, 20));
        }


        private void ReceiveData(IAsyncResult ar)
        {
            Socket socket = (Socket)ar.AsyncState;
            if (socket.Connected)
            {
                int received = socket.EndReceive(ar);
                byte[] dataBuf = new byte[received];
                Array.Copy(receivedBuf, dataBuf, received);
                string text = Encoding.ASCII.GetString(dataBuf);

                switch (text)
                {
                    case "Capture":
                        capture();
                        break;

                    case "Monitor":
                        t1 = new Thread(new ThreadStart(Monitor));
                        t1.Start();
                        break;

                    //case "Record":
                    //    t2 = new Thread(new ThreadStart(rec));
                    //    t2.Start();
                    //    break;

                    //case "Stop_Record":
                    //    t2.Abort();
                    //    break;

                    case "Stop":
                        t1.Abort();
                        break;

                    case "Shutdown":
                        Command_operation("shutdown.exe", "-s");
                        break;

                    case "Lock":
                        Command_operation("Rundll32.exe", "User32.dll, LockWorkStation");
                        break;

                    case "Restart":
                        Command_operation("shutdown.exe", "-r");
                        break;

                    default:
                        MsgBox.Invoke((MethodInvoker)(() => MsgBox.AppendText(text + "\r\n")));
                        break;

                }

                _clientSocket.BeginReceive(receivedBuf, 0, receivedBuf.Length, SocketFlags.None, new AsyncCallback(ReceiveData), _clientSocket);
            }
        }


        private void Command_operation(string filename, string arguments)
        {
            ProcessStartInfo startinfo = new ProcessStartInfo(filename, arguments);
            Process.Start(startinfo);
        }

        private void Monitor()
        {
            while (true)
            {
                try
                {
                    capture();
                    Thread.Sleep(100);
                }
                catch (Exception) { }
            }

        }

        private void capture()
        {

            Bitmap memoryImage = new Bitmap(1920, 1080);
            Size s = new Size(memoryImage.Width, memoryImage.Height);

            Graphics memoryGraphics = Graphics.FromImage(memoryImage);

            memoryGraphics.CopyFromScreen(0, 0, 0, 0, s);

            _clientSocket.Send(converterDemo(memoryImage));

        }

        public static byte[] converterDemo(Image x)
        {
            ImageConverter _imageConverter = new ImageConverter();
            byte[] xByte = (byte[])_imageConverter.ConvertTo(x, typeof(byte[]));
            return xByte;
        }

        private void LoopConnect()
        {
            //int attempts = 0;
            try
            {
                //attempts++;
                _clientSocket.Connect(IPAddress.Parse(IP_Address.Text), 100);
                MessageBox.Show("       Connection Successful !!     ");
                IP_Address.Enabled = false;
            }
            catch (Exception)
            {
                MessageBox.Show(" Connection Failed ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

           
        }
        private void Rounded_TextBox(TextBox txtBox, int height, int width)
        {
            txtBox.BorderStyle = BorderStyle.None;
            txtBox.AutoSize = false;
            txtBox.Region = Region.FromHrgn(CreateRoundRectRgn(0, 0, width, height, 10, 10));
        }

        private void sideBarTimer_Tick(object sender, EventArgs e)
        {
            if (sidebarExpand)
            {
              //  sideBar.BackColor = Color.FromArgb(23, 20, 18);
                sideBar.Width -= 40;
                sideBar.Height -= 40;

                if (sideBar.Width == sideBar.MinimumSize.Width && sideBar.Height == sideBar.MinimumSize.Height)
                {
                    sidebarExpand = false;
                    sideBarTimer.Stop();
                }
            }
            else
            {
                //sideBar.BackColor = Color.FromArgb(23, 20, 18);
                sideBar.Height += 40;
                sideBar.Width += 40;

                if (sideBar.Width == sideBar.MaximumSize.Width && sideBar.Height == sideBar.MaximumSize.Height)
                {
                    sidebarExpand = true;
                    sideBarTimer.Stop();
                }
            }
        }

        private void Menu_btn_Click(object sender, MouseEventArgs e)
        {
            sideBarTimer.Start();          
        }

        private void BtnDisconnect_Hover(object sender, EventArgs e)
        {
            BtnDisconnect_Hover_ColorChange();
        }
        private void BtnDisconnect_Leave(object sender, EventArgs e)
        {
            BtnDisconnect_Leave_ColorChange();
        }

        private void BtnDisconnect_Hover_ColorChange()
        {
            Disconnect_btn.BackColor = Color.WhiteSmoke;
            Disconnect_btn.ForeColor = Color.Red;
        }

        private void BtnDisconnect_Leave_ColorChange()
        {
            Disconnect_btn.BackColor = Color.Black;
            Disconnect_btn.ForeColor = Color.SeaShell;
        }

        private void BtnConnect_Hover_ColorChange()
        {
            Connect_btn.BackColor = Color.WhiteSmoke;
            Connect_btn.ForeColor = Color.Green;

        }

        private void BtnConnect_Leave_ColorChange()
        {
            Connect_btn.BackColor = Color.Black;
            Connect_btn.ForeColor = Color.SeaShell;

        }
        private void BtnConnect_Hover(object sender, EventArgs e)
        {
            BtnConnect_Hover_ColorChange();
        }

        private void BtnConnect_Leave(object sender, EventArgs e)
        {
            BtnConnect_Leave_ColorChange();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            chatBox.Text = "\n\n  Message";
            txtName.Text = "Name";
            txtRollno.Text = "Roll no.";
            IP_Address.Text = "\nIPv4";
          
        }

        private void chatBox_Enter(object sender, EventArgs e)
        {
            if (chatBox.Text == "\n\n  Message")
            {
                chatBox.Text = "";
            }
        }
        private void chatBox_Leave(object sender, EventArgs e)
        {
            if (chatBox.Text == "")
            {
                chatBox.Text = "\n\n  Message";
            }
        }

        private void Changing_Theme(object sender, EventArgs e)
        {
            if (light_mode.Checked)
            {
                MsgBox.BackColor = Color.WhiteSmoke;
                chatBox.BackColor = Color.WhiteSmoke;
                MsgBox.ForeColor = Color.Black;
                chatBox.ForeColor = Color.Black;
                header.BackColor = Color.WhiteSmoke;
                lbName.ForeColor = Color.Black;
                sideBar.BackColor = Color.WhiteSmoke;
                Menu_btn.BackgroundImage = Properties.Resources.menu_light_theme;
               
            }
            else
            {
                MsgBox.BackColor = Color.FromArgb(34, 34, 34);
                MsgBox.ForeColor = Color.WhiteSmoke;
                chatBox.BackColor = Color.FromArgb(34, 34, 34);
                chatBox.ForeColor = Color.WhiteSmoke;
                header.BackColor = Color.FromArgb(20, 20, 20);
                lbName.ForeColor = Color.SeaShell;
                sideBar.BackColor = Color.FromArgb(23, 20, 18);
                Menu_btn.BackgroundImage = Properties.Resources.menu_dark_theme;

            }
        }

        private void txtName_Enter(object sender, EventArgs e)
        {
            if (txtName.Text == "Name")
            {
                txtName.Text = "";
                txtName.BackColor = Color.SeaShell;
                txtName.ForeColor = Color.Black;
                txtName.TextAlign = HorizontalAlignment.Center;
            }
        }

        private void txtRollno_Enter(object sender, EventArgs e)
        {
            if (txtRollno.Text == "Roll no.")
            {
                txtRollno.Text = "";
                txtRollno.BackColor = Color.SeaShell;
                txtRollno.ForeColor = Color.Black;
                txtRollno.TextAlign = HorizontalAlignment.Center;

            }

        }

        private void txtName_Leave(object sender, EventArgs e)
        {
            if (txtName.Text == "")
            {
                txtName.Text = "Name";
                txtName.BackColor = Color.FromArgb(51, 51, 51);
                txtName.ForeColor = Color.SeaShell;
                txtName.TextAlign = HorizontalAlignment.Left;
            }
        }

        private void txtRollno_Leave(object sender, EventArgs e)
        {
            if (txtRollno.Text == "")
            {
                txtRollno.Text = "Roll no.";
                txtRollno.BackColor = Color.FromArgb(51, 51, 51);
                txtRollno.ForeColor = Color.SeaShell;
                txtRollno.TextAlign = HorizontalAlignment.Left;
            }
        }

        private void Submit(object sender, EventArgs e)
        {

            if (txtName.Text == "Name" && txtRollno.Text == "Roll no." || txtName.Text == "Name" && txtRollno.Text == "" || txtName.Text == "" && txtRollno.Text == "Roll no.")
            {
                
                MessageBox.Show("Please enter your Details", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else if (txtName.Text == "Name" && txtRollno.Text != "Roll no." || txtName.Text == "" && txtRollno.Text != "Roll no.")
            {
                MessageBox.Show("Please enter your Name ", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (txtName.Text != "Name" && txtRollno.Text == "Roll no." || txtName.Text != "Name" && txtRollno.Text == "")
            {
                MessageBox.Show("Please enter your Roll no. ", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                panel259.Hide();
                txtName.Hide();
                txtRollno.Hide();
                pictureBox1.Hide();
                pictureBox2.Hide();
                pictureBox3.Hide();
                lbLogin.Hide();
                panel2.Hide();
                panel260.Hide();
                panel261.Hide();

                string[] name = txtName.Text.Split(' ');
                lbName.Text = "Hii  " + name[0].ToUpper();

            }
        }

        private void Submit_Hover(object sender, EventArgs e)
        {
            Bitmap b = new Bitmap(Properties.Resources.Hover_Arrow);
            pictureBox1.Image = b;
        }

        private void Submit_Leave(object sender, EventArgs e)
        {
            Bitmap b = new Bitmap(Properties.Resources.submit1);
            pictureBox1.Image = b;
        }

        private void txtRollno_keypressed(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtName_Keypresed(object sender, KeyPressEventArgs e)
        {
            if(!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void IP_Address_Enter(object sender, EventArgs e)
        {
            if (IP_Address.Text == "\nIPv4")
            {
                IP_Address.Text = "";
            }
        }

        private void IP_Address_Leave(object sender, EventArgs e)
        {
            if (IP_Address.Text == "")
            {
                IP_Address.Text = "\nIPv4";
            }
        }

        private void Connect_btn_Click(object sender, EventArgs e)
        {
          
            if(!_clientSocket.Connected)
            {
                string name = $"{txtName.Text},{txtRollno.Text},Connect";
                LoopConnect();

                _clientSocket.BeginReceive(receivedBuf, 0, receivedBuf.Length, SocketFlags.None, new AsyncCallback(ReceiveData), _clientSocket);

                byte[] buffer = Encoding.ASCII.GetBytes(name);
                _clientSocket.Send(buffer);
            }
            
        }

        private void close_connection()
        {

            _clientSocket.Shutdown(SocketShutdown.Both);
            _clientSocket.Close();
            _clientSocket = null;

        }
        private void Disconnect_btn_Click(object sender, EventArgs e)
        {
            if(_clientSocket.Connected)
            {
                //Disconnect_btn.Enabled = false;
                byte[] buffer = Encoding.ASCII.GetBytes($"{txtName.Text.ToUpper()} : Closed Connection");
                _clientSocket.Send(buffer);
                _clientSocket.Shutdown(SocketShutdown.Both);
            }
           
        }

        private void sendBtn_Click(object sender, EventArgs e)
        {
            if (_clientSocket.Connected)
            {
                if(chatBox.Text != "\n\n  Message" && chatBox.Text != "")
                {
                    byte[] buffer = Encoding.ASCII.GetBytes(txtName.Text.ToUpper() + " : " + chatBox.Text);
                    _clientSocket.Send(buffer);
                    MsgBox.AppendText("You : " + chatBox.Text + "\r\n");
                    chatBox.Text = "";
                }               
            }
        }

        private void Client_CLose(object sender, FormClosingEventArgs e)
        {

            if (_clientSocket.Connected)
            {
                byte[] buffer = Encoding.ASCII.GetBytes($"{txtName.Text.ToUpper()} : Closed Connection");
                _clientSocket.Send(buffer);
                close_connection();
            }
            else
            {
                _clientSocket.Close();
                _clientSocket = null;
            }
        }


        [StructLayout(LayoutKind.Sequential)]
        public struct DEV_BROADCAST_VOLUME
        {
            public int dbcv_size;
            public int dbcv_devicetype;
            public int dbcv_reserved;
            public int dbcv_unitmask;
        }

        protected override void WndProc(ref Message m)
        {

            switch (m.Msg)
            {
                case WM_DEVICECHANGE:
                    switch ((int)m.WParam)
                    {
                        case DBT_DEVICEARRIVAL:
                            
                                int devtype = Marshal.ReadInt32(m.LParam, 4);
                                if (devtype == DBT_DEVTYPVOLUME)
                                {
                                    do
                                    {
                                        byte[] USB_I = Encoding.ASCII.GetBytes($"{txtName.Text.ToUpper()} : INSERTED USB");
                                            _clientSocket.Send(USB_I);
                                             DEV_BROADCAST_VOLUME vol;
                                            vol = (DEV_BROADCAST_VOLUME)Marshal.PtrToStructure(m.LParam, typeof(DEV_BROADCAST_VOLUME));
                                            MessageBox.Show(" Remove USB ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            Command_operation("Rundll32.exe", "User32.dll, LockWorkStation");
                                    } while ((int)m.WParam == (DBT_DEVICEREMOVALCOMPLETE)) ;
                                   //MessageBox.Show("USB Inserted");                           
                                } 
                            
                            break;

                        case DBT_DEVICEREMOVALCOMPLETE:
                            byte[] buffer = Encoding.ASCII.GetBytes($"{txtName.Text.ToUpper()} : REMOVED USB");
                            _clientSocket.Send(buffer);
                            //MessageBox.Show("usb out");
                            break;
                    }
                    break;
            }
            base.WndProc(ref m);
        }


        //private void rec()
        //{
        //    VideoFileWriter writer = new VideoFileWriter();
        //    writer.Open($@"D:\{txtName.Text.ToUpper()}_Recording_{DateTime.Now.ToString(" dd_MMMM_hh_mm_ss_tt ")}.mp4", 1920, 1080, 25, VideoCodec.MPEG4, 800000);

        //    while (true)
        //    {
        //        using (Bitmap bitmap = new Bitmap(1920, 1080))
        //        {
        //            Size s = new Size(bitmap.Width, bitmap.Height);
        //            using (Graphics g = Graphics.FromImage(bitmap))
        //            {
        //                g.CopyFromScreen(0, 0, 0, 0, s);
        //            }
        //            writer.WriteVideoFrame(bitmap);
        //        }
        //    }
        //}

        private void IP_Address_Pressed(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }
    }
}