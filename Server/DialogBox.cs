﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    public partial class DialogBox : Form
    {

        public TextBox ip;
        public TextBox user;
        public TextBox pass;
        public static DialogBox instance;

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
       (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
       );


        public DialogBox()
        {
            InitializeComponent();

            ip = IPv4;
            user = username;
            pass = password; 
            instance = this;

            Rounded_TextBox(IPv4, 30, 180);
            Rounded_TextBox(username, 30, 180);
            Rounded_TextBox(password, 30, 180);

            //ConnectBtn.AutoSize = false;
            //ConnectBtn.Region = Region.FromHrgn(CreateRoundRectRgn(5, 5, 90, 40, 10, 10));
        }


        private void Rounded_TextBox(TextBox txtBox, int height, int width)
        {
            txtBox.BorderStyle = BorderStyle.None;
            txtBox.AutoSize = false;
            txtBox.Region = Region.FromHrgn(CreateRoundRectRgn(0, 0, width, height, 10, 10));
        }

        private void ConnectBtn_Hover(object sender, EventArgs e)
        {

            ConnectBtn.BackColor = Color.Black;
            ConnectBtn.ForeColor = Color.SeaShell;
        }

        private void ConnectBtn_Leave(object sender, EventArgs e)
        {
            ConnectBtn.BackColor = Color.SeaShell;
            ConnectBtn.ForeColor = Color.Black;

        }

        private void ConnectBtn_Click(object sender, EventArgs e)
        {
            if (IPv4.Text == "" && user.Text == ""  && pass.Text == "" || IPv4.Text == "" && user.Text == "" || user.Text == "" && pass.Text == "" || IPv4.Text == "" && pass.Text == "" || IPv4.Text == "" || pass.Text == "" || user.Text == "")
            {
                MessageBox.Show("Please fill all Details", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                this.Hide();
                RemoteDesktop rd = new RemoteDesktop();
                rd.Show();
            }            
        }

        private void IPv4_KeyPressed(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }
    }
}
