﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accord.Video.FFMPEG;


namespace Server
{
    public partial class Monitor : Form
    {

        bool sidebarExpand;
        public static Monitor instance;
        public PictureBox pic1;

        private VideoFileWriter _writer = new VideoFileWriter();


        public Monitor()
        {
            InitializeComponent();
            instance = this;
            pic1 = Screen;
            
        }

        private void Monitor_Load(object sender, EventArgs e)
        {
            //StopMonitoringBtn.Enabled = false;
            StopRecordBtn.Enabled = false;
        }


        private void Menu_timer_tick(object sender, EventArgs e)
        {
            if (sidebarExpand)
            {
                //  sideBar.BackColor = Color.FromArgb(23, 20, 18);
                MenuPanel.Width -= 60;
                MenuPanel.Height -= 60;

                if (MenuPanel.Width == MenuPanel.MinimumSize.Width && MenuPanel.Height == MenuPanel.MinimumSize.Height)
                {
                    sidebarExpand = false;
                    Menu_timer.Stop();
                }
            }
            else
            {
                //sideBar.BackColor = Color.FromArgb(23, 20, 18);
                MenuPanel.Height += 60;
                MenuPanel.Width += 60;

                if (MenuPanel.Width == MenuPanel.MaximumSize.Width && MenuPanel.Height == MenuPanel.MaximumSize.Height)
                {
                    sidebarExpand = true;
                    Menu_timer.Stop();
                }
            }
        }

        private void MenuBtn_Click(object sender, EventArgs e)
        {
            Menu_timer.Start();
        }

        private void MonitorBtn_Click(object sender, EventArgs e)
        {
            if(Screen.Image == null)
            {
                Server.instance.SendMsg("Monitor");
            }
            //MonitorBtn.Enabled = false;
            //StopMonitoringBtn.Enabled = true;
            
        }

        private void StopMonitoringBtn_Click(object sender, EventArgs e)
        {
            if(Screen.Image != null)
            {
                Server.instance.SendMsg("Stop");
                Screen.Image = null;
            }
           // MonitorBtn.Enabled = true;
           
            
        }

        private void CaptureBtn_Click(object sender, EventArgs e)
        {
            if(Screen.Image != null)
            {
                SaveFileDialog sf = new SaveFileDialog();
                sf.Filter = "PNG(*.PNG)|*.png|JPG(*.JPG)|*.jpg";

                sf.FileName = @"Screenshot" + "_" + DateTime.Now.ToString(" dd_MMMM_hh_mm_ss_tt ");

                if (sf.ShowDialog() == DialogResult.OK)
                {
                    Screen.Image.Save(sf.FileName);
                }
            }            
        }

        private void Monitor_Close(object sender, FormClosingEventArgs e)
        {
            Server.instance.b1.Enabled = true;
            if(Screen.Image != null)
            {
                Server.instance.SendMsg("Stop");
            }
            
        }

        private void Recording_timer_tick(object sender, EventArgs e)
        {
            Bitmap memoryImage = new Bitmap(1920, 1080);
            Size s = new Size(memoryImage.Width, memoryImage.Height);

            Graphics memoryGraphics = Graphics.FromImage(memoryImage);

            memoryGraphics.CopyFromScreen(0, 0, 0, 0, s);

            _writer.WriteVideoFrame(memoryImage);
        }

        private void RecordingBtn_Click(object sender, EventArgs e)
        {
            if(Screen.Image != null)
            {
                RecordingBtn.Enabled = false;
                StopRecordBtn.Enabled = true;
                var dialog = new SaveFileDialog();
                dialog.Filter = "MP4|*.mp4|WMV|*.wmv|AVI|*.avi";

                dialog.FileName = "Recording" + "_" + DateTime.Now.ToString(" dd_MMMM_hh_mm_ss_tt ");

                dialog.AddExtension = true;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    _writer.Open(dialog.FileName, 1920, 1080, 23, VideoCodec.MPEG4, 800000);

                    Bitmap memoryImage = new Bitmap(1920, 1080);
                    Size s = new Size(memoryImage.Width, memoryImage.Height);

                    Graphics memoryGraphics = Graphics.FromImage(memoryImage);

                    memoryGraphics.CopyFromScreen(0, 0, 0, 0, s);

                    _writer.WriteVideoFrame(memoryImage);
                }
                else
                {
                    RecordingBtn.Enabled = true;
                    StopRecordBtn.Enabled = false;
                }
            }
            
        }

        private void StopRecordBtn_Click(object sender, EventArgs e)
        {
            
            RecordingBtn.Enabled = true;
            _writer.Close();
            MessageBox.Show("Video Recorded", "Successfully", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
