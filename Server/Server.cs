﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Net.Sockets;
using System.Net;

namespace Server
{
    public partial class Server : Form
    {

        public static Server instance;
        public Socket _serversocket;
        private List<SocketT2h> _clientsockets = new List<SocketT2h>();
        private byte[] _buffer = new byte[1716964];
        List<string> Student_Name = new List<string>();

        public Button b1;
       

        public CheckedListBox clb;

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        public Server()
        {
            InitializeComponent();
        

            _serversocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            instance = this;
            b1 = MonitorBtn;
           
            clb = Checked_list;


            Rounded_TextBox(MsgBox, 426, 285);
            Rounded_TextBox(ChatBox, 46, 234);
            panel1.BorderStyle = BorderStyle.None;
            panel1.AutoSize = false;
            panel1.Height = 612;
            panel1.Width = 1202;
            panel1.Region = Region.FromHrgn(CreateRoundRectRgn(10, 10, panel1.Width, panel1.Height, 20, 20));

            Table.BorderStyle = BorderStyle.None;
            Table.AutoSize = false;
            Table.Height = 483;
            Table.Width = 457;
            Table.Region = Region.FromHrgn(CreateRoundRectRgn(5, 5, Table.Width, Table.Height, 10, 10));
        }


        private void Rounded_TextBox(TextBox txtBox, int height, int width)
        {
            txtBox.BorderStyle = BorderStyle.None;
            txtBox.AutoSize = false;
            txtBox.Region = Region.FromHrgn(CreateRoundRectRgn(0, 0, width, height, 30, 30));
        }

        private void Homebtn_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if(button.Location.Y > FLoating_panel.Location.Y)
            {
                for (int i = 0; i < button.Height; i += 5)
                {
                    FLoating_panel.Location = new Point(button.Location.X, button.Location.Y - 43 + i);
                    FLoating_panel.Size = new Size(5, 45);
                    Thread.Sleep(5);
                }
            }
            else
            {
                for (int i = 0; i < button.Height; i += 5)
                {
                    FLoating_panel.Location = new Point(button.Location.X, button.Location.Y + 47 - i);
                    FLoating_panel.Size = new Size(5, 45);
                    Thread.Sleep(5);
                }
            }
            button.BackColor = Color.FromArgb(0,0,0);

            
        }

        private void Server_Load(object sender, EventArgs e)
        {
           
            ChatBox.Text = "\n\n  Message";
            SetupServer();
        }

        private void SetupServer()
        {

            _serversocket.Bind(new IPEndPoint(IPAddress.Any, 100));
            _serversocket.Listen(1);
            _serversocket.BeginAccept(new AsyncCallback(AppceptCallback), null);
        }


        private void AppceptCallback(IAsyncResult ar)
        {
            Socket socket = _serversocket.EndAccept(ar);
            _clientsockets.Add(new SocketT2h(socket));

            socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), socket);
            _serversocket.BeginAccept(new AsyncCallback(AppceptCallback), null);

        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            Socket socket = (Socket)ar.AsyncState;

            if (socket.Connected)
            {
                int received;
                try
                {
                    received = socket.EndReceive(ar);
                }
                catch (Exception)
                {
                    for (int i = 0; i < _clientsockets.Count; i++)
                    {
                        if (_clientsockets[i]._Socket.RemoteEndPoint.ToString().Equals(socket.RemoteEndPoint.ToString()))
                        {
                            _clientsockets.RemoveAt(i);
                        }
                    }
                    return;
                }

                if (received != 0)
                {
                    byte[] dataBuf = new byte[received];
                    Array.Copy(_buffer, dataBuf, received);

                    //try
                    //{

                    //    ScreenShot.instance.pic.Invoke((MethodInvoker)(() => ScreenShot.instance.pic.Image =
                    //                                                    (Bitmap)(new ImageConverter()).ConvertFrom(dataBuf)));
                    //}
                    //catch (Exception) { }

                    try
                    {
                        Monitor.instance.pic1.Invoke((MethodInvoker)(() => Monitor.instance.pic1.Image =
                                                                            (Bitmap)(new ImageConverter()).ConvertFrom(dataBuf)));
                    }
                    catch (Exception) { }

                    string text = Encoding.ASCII.GetString(dataBuf);

                    string[] client_details = text.Split(',');
                    Student_Name.Add(client_details[0].ToUpper() + " ");

                    string address = (string)socket.RemoteEndPoint.ToString();
                    string[] ip_port = address.Split(':');

                    string[] name = text.Split(':');


                    try
                    {
                        if (client_details[2].Equals("Connect"))
                        {
                            //socket.RemoteEndPoint.ToString() + ":" + 
                            Checked_list.Invoke((MethodInvoker)(() => Checked_list.Items.Add(client_details[0].ToUpper() + " : " + client_details[1])));
                            Table.Invoke((MethodInvoker)(() => Table.Rows.Add(client_details[0].ToUpper(), client_details[1],ip_port[1], ip_port[0])));
                        }
                    }
                    catch (Exception)
                    {

                        List<string> myList = Student_Name.Distinct().ToList();
                        foreach (string obj in myList)
                        {
                            if (name[0].Equals(obj))
                            {
                                MsgBox.Invoke((MethodInvoker)(() => MsgBox.AppendText(text + "\r\n")));
                            }
                        }
                    }


                    try
                    {
                        switch (name[1])
                        {
                            case " INSERTED USB":
                                //this.ShowDialog();
                                MessageBox.Show(text, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                break;

                            case " REMOVED USB":
                                MessageBox.Show(text, "Alert", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                break;
                        }
                    }
                    catch (Exception) { }

                }
                else
                {
                    for (int i = 0; i < _clientsockets.Count; i++)
                    {
                        if (_clientsockets[i]._Socket.RemoteEndPoint.ToString().Equals(socket.RemoteEndPoint.ToString()))
                        {
                            _clientsockets.RemoveAt(i);
                            Table.Invoke((MethodInvoker)(() => Table.Rows.RemoveAt(i)));
                            Checked_list.Invoke((MethodInvoker)(() => Checked_list.Items.RemoveAt(i)));
                        }
                    }
                }
            }

            socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), socket);

        }

        public void Sendata(Socket socket, string text)
        {

            byte[] data = Encoding.ASCII.GetBytes(text);
            socket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(SendCallback), socket);
            _serversocket.BeginAccept(new AsyncCallback(AppceptCallback), null);
        }

        private void SendCallback(IAsyncResult ar)
        {
            Socket socket = (Socket)ar.AsyncState;
            socket.EndSend(ar);
        }


        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void LockBtn_Click(object sender, EventArgs e)
        {
            SendMsg("Lock");
        }

        private void ChatBox_Enter(object sender, EventArgs e)
        {
            if (ChatBox.Text == "\n\n  Message")
            {
                ChatBox.Text = "";
            }
        }

        private void ChatBox_Leave(object sender, EventArgs e)
        {
            if (ChatBox.Text == "")
            {
                ChatBox.Text = "\n\n  Message";
            }
        }

        private void RestartBtn_Click(object sender, EventArgs e)
        {
            SendMsg("Restart");
        }

        private void ShutdownBtn_Click(object sender, EventArgs e)
        {
            SendMsg("Shutdown");
        }

        private void Monitorbtn_Click(object sender, EventArgs e)
        {
            
            if(Checked_list.CheckedItems.Count == 1)
            {
                Button button = (Button)sender;

                if (button.Location.Y > FLoating_panel.Location.Y)
                {
                    for (int i = 0; i < button.Height; i += 5)
                    {
                        FLoating_panel.Location = new Point(button.Location.X, button.Location.Y - 43 + i);
                        FLoating_panel.Size = new Size(5, 45);
                        Thread.Sleep(5);
                    }
                }
                else
                {
                    for (int i = 0; i < button.Height; i += 5)
                    {
                        FLoating_panel.Location = new Point(button.Location.X, button.Location.Y + 47 - i);
                        FLoating_panel.Size = new Size(5, 45);
                        Thread.Sleep(5);
                    }
                }
                button.BackColor = Color.FromArgb(0, 0, 0);

                Monitor m = new Monitor();
                m.Show();
            }
            else if(Checked_list.CheckedItems.Count == 0)
            {
               // MessageBox.Show("", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("You can only monitor one PC at a time","",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            
        }

        private void Remotebtn_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (button.Location.Y > FLoating_panel.Location.Y)
            {
                for (int i = 0; i < button.Height; i += 5)
                {
                    FLoating_panel.Location = new Point(button.Location.X, button.Location.Y - 43 + i);
                    FLoating_panel.Size = new Size(5, 45);
                    Thread.Sleep(5);
                }
            }
            else
            {
                for (int i = 0; i < button.Height; i += 5)
                {
                    FLoating_panel.Location = new Point(button.Location.X, button.Location.Y + 47 - i);
                    FLoating_panel.Size = new Size(5, 45);
                    Thread.Sleep(5);
                }
            }
            button.BackColor = Color.FromArgb(0, 0, 0);

            DialogBox db = new DialogBox();
            db.Show();
        }



        // --------------------------------Download the list of Client in Table as a PDF ------------------------------------------

        private void DownloadBtn_Click(object sender, EventArgs e)
        {
            if (Table.Rows.Count > 0)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "PDF (*.pdf)|*.pdf";
                sfd.FileName = "Conneted_Students.pdf";
                bool fileError = false;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(sfd.FileName))
                    {
                        try
                        {
                            File.Delete(sfd.FileName);
                        }
                        catch (IOException ex)
                        {
                            fileError = true;
                            MessageBox.Show("It wasn't possible to write the data to the disk." + ex.Message);
                        }
                    }
                    if (!fileError)
                    {
                        try
                        {
                            PdfPTable pdfTable = new PdfPTable(Table.Columns.Count);
                            pdfTable.DefaultCell.Padding = 3;
                            pdfTable.WidthPercentage = 100;
                            pdfTable.HorizontalAlignment = Element.ALIGN_LEFT;

                            foreach (DataGridViewColumn column in Table.Columns)
                            {
                                PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                                pdfTable.AddCell(cell);
                            }

                            foreach (DataGridViewRow row in Table.Rows)
                            {
                                foreach (DataGridViewCell cell in row.Cells)
                                {
                                    pdfTable.AddCell(cell.Value.ToString());
                                }
                            }

                            using (FileStream stream = new FileStream(sfd.FileName, FileMode.Create))
                            {
                                Document pdfDoc = new Document(PageSize.A4, 10f, 20f, 20f, 10f);
                                PdfWriter.GetInstance(pdfDoc, stream);
                                pdfDoc.Open();
                                pdfDoc.Add(pdfTable);
                                pdfDoc.Close();
                                stream.Close();
                            }

                            MessageBox.Show("Data Exported Successfully !!!", "Info",MessageBoxButtons.OK,MessageBoxIcon.Information);
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No Record To Export !!!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        // ---------------------------------------------------------------------------------------------------------

        private void SendBtn_Click(object sender, EventArgs e)
        {
           if(Checked_list.CheckedItems.Count >= 1)
            {
                if (ChatBox.Text != "\n\n  Message" && ChatBox.Text != "")
                {
                    SendMsg("Server : " + ChatBox.Text);
                    MsgBox.AppendText("You : " + ChatBox.Text + "\r\n");
                    ChatBox.Text = "";
                }
            }      
        }

        public void SendMsg(string text)
        {
            List<int> list = new List<int>();

            foreach (int index in Checked_list.CheckedIndices)
            {
                list.Add(index);
            }

            for (int i = 0; i < Checked_list.CheckedItems.Count; i++)
            {
                Sendata(_clientsockets[list[i]]._Socket, text);
            }

        }

     
    }
}
