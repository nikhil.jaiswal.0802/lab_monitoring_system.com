﻿namespace Server
{
    partial class Server
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Server));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.DownloadBtn = new System.Windows.Forms.Button();
            this.Table = new System.Windows.Forms.DataGridView();
            this.StdName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rollno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PORT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Send_btn = new System.Windows.Forms.Panel();
            this.Checked_list = new System.Windows.Forms.CheckedListBox();
            this.ChatBox = new System.Windows.Forms.TextBox();
            this.MsgBox = new System.Windows.Forms.TextBox();
            this.FLoating_panel = new System.Windows.Forms.Panel();
            this.ShutdownBtn = new System.Windows.Forms.Button();
            this.RestartBtn = new System.Windows.Forms.Button();
            this.LockBtn = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.MonitorBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(24)))), ((int)(((byte)(24)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.DownloadBtn);
            this.panel1.Controls.Add(this.Table);
            this.panel1.Controls.Add(this.Send_btn);
            this.panel1.Controls.Add(this.Checked_list);
            this.panel1.Controls.Add(this.ChatBox);
            this.panel1.Controls.Add(this.MsgBox);
            this.panel1.Location = new System.Drawing.Point(68, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(994, 571);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel1_Paint);
            // 
            // DownloadBtn
            // 
            this.DownloadBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.DownloadBtn.Image = ((System.Drawing.Image)(resources.GetObject("DownloadBtn.Image")));
            this.DownloadBtn.Location = new System.Drawing.Point(441, 508);
            this.DownloadBtn.Name = "DownloadBtn";
            this.DownloadBtn.Size = new System.Drawing.Size(44, 46);
            this.DownloadBtn.TabIndex = 11;
            this.DownloadBtn.UseVisualStyleBackColor = true;
            this.DownloadBtn.Click += new System.EventHandler(this.DownloadBtn_Click);
            // 
            // Table
            // 
            this.Table.AllowUserToAddRows = false;
            this.Table.AllowUserToDeleteRows = false;
            this.Table.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(24)))), ((int)(((byte)(24)))));
            this.Table.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(24)))), ((int)(((byte)(24)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(188)))), ((int)(((byte)(215)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Table.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Table.ColumnHeadersHeight = 45;
            this.Table.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StdName,
            this.Rollno,
            this.PORT,
            this.IP});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Table.DefaultCellStyle = dataGridViewCellStyle2;
            this.Table.Location = new System.Drawing.Point(20, 45);
            this.Table.Name = "Table";
            this.Table.ReadOnly = true;
            this.Table.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.Table.RowTemplate.Height = 24;
            this.Table.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Table.Size = new System.Drawing.Size(510, 457);
            this.Table.TabIndex = 10;
            // 
            // StdName
            // 
            this.StdName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StdName.FillWeight = 50F;
            this.StdName.HeaderText = "NAME";
            this.StdName.Name = "StdName";
            this.StdName.ReadOnly = true;
            // 
            // Rollno
            // 
            this.Rollno.HeaderText = "ROLL NO.";
            this.Rollno.Name = "Rollno";
            this.Rollno.ReadOnly = true;
            // 
            // PORT
            // 
            this.PORT.HeaderText = "PORT";
            this.PORT.Name = "PORT";
            this.PORT.ReadOnly = true;
            this.PORT.Width = 80;
            // 
            // IP
            // 
            this.IP.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.IP.FillWeight = 64.61937F;
            this.IP.HeaderText = "IPv4";
            this.IP.Name = "IP";
            this.IP.ReadOnly = true;
            // 
            // Send_btn
            // 
            this.Send_btn.BackgroundImage = global::Server.Properties.Resources.send;
            this.Send_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Send_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Send_btn.Location = new System.Drawing.Point(910, 501);
            this.Send_btn.Name = "Send_btn";
            this.Send_btn.Size = new System.Drawing.Size(54, 46);
            this.Send_btn.TabIndex = 6;
            this.Send_btn.Click += new System.EventHandler(this.SendBtn_Click);
            // 
            // Checked_list
            // 
            this.Checked_list.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(24)))), ((int)(((byte)(24)))));
            this.Checked_list.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Checked_list.CheckOnClick = true;
            this.Checked_list.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Checked_list.ForeColor = System.Drawing.Color.SeaShell;
            this.Checked_list.FormattingEnabled = true;
            this.Checked_list.Location = new System.Drawing.Point(523, 63);
            this.Checked_list.Name = "Checked_list";
            this.Checked_list.Size = new System.Drawing.Size(150, 396);
            this.Checked_list.TabIndex = 4;
            // 
            // ChatBox
            // 
            this.ChatBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.ChatBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ChatBox.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChatBox.ForeColor = System.Drawing.Color.SeaShell;
            this.ChatBox.Location = new System.Drawing.Point(679, 501);
            this.ChatBox.Multiline = true;
            this.ChatBox.Name = "ChatBox";
            this.ChatBox.Size = new System.Drawing.Size(234, 46);
            this.ChatBox.TabIndex = 2;
            this.ChatBox.Enter += new System.EventHandler(this.ChatBox_Enter);
            this.ChatBox.Leave += new System.EventHandler(this.ChatBox_Leave);
            // 
            // MsgBox
            // 
            this.MsgBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.MsgBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MsgBox.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MsgBox.ForeColor = System.Drawing.Color.SeaShell;
            this.MsgBox.Location = new System.Drawing.Point(679, 54);
            this.MsgBox.Multiline = true;
            this.MsgBox.Name = "MsgBox";
            this.MsgBox.ReadOnly = true;
            this.MsgBox.Size = new System.Drawing.Size(285, 426);
            this.MsgBox.TabIndex = 0;
            // 
            // FLoating_panel
            // 
            this.FLoating_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(188)))), ((int)(((byte)(215)))));
            this.FLoating_panel.Location = new System.Drawing.Point(12, 175);
            this.FLoating_panel.Name = "FLoating_panel";
            this.FLoating_panel.Size = new System.Drawing.Size(5, 40);
            this.FLoating_panel.TabIndex = 4;
            // 
            // ShutdownBtn
            // 
            this.ShutdownBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ShutdownBtn.Image = ((System.Drawing.Image)(resources.GetObject("ShutdownBtn.Image")));
            this.ShutdownBtn.Location = new System.Drawing.Point(19, 557);
            this.ShutdownBtn.Name = "ShutdownBtn";
            this.ShutdownBtn.Size = new System.Drawing.Size(43, 42);
            this.ShutdownBtn.TabIndex = 7;
            this.ShutdownBtn.UseVisualStyleBackColor = true;
            this.ShutdownBtn.Click += new System.EventHandler(this.ShutdownBtn_Click);
            // 
            // RestartBtn
            // 
            this.RestartBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RestartBtn.Image = ((System.Drawing.Image)(resources.GetObject("RestartBtn.Image")));
            this.RestartBtn.Location = new System.Drawing.Point(19, 509);
            this.RestartBtn.Name = "RestartBtn";
            this.RestartBtn.Size = new System.Drawing.Size(43, 42);
            this.RestartBtn.TabIndex = 6;
            this.RestartBtn.UseVisualStyleBackColor = true;
            this.RestartBtn.Click += new System.EventHandler(this.RestartBtn_Click);
            // 
            // LockBtn
            // 
            this.LockBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LockBtn.Image = ((System.Drawing.Image)(resources.GetObject("LockBtn.Image")));
            this.LockBtn.Location = new System.Drawing.Point(19, 461);
            this.LockBtn.Name = "LockBtn";
            this.LockBtn.Size = new System.Drawing.Size(43, 42);
            this.LockBtn.TabIndex = 5;
            this.LockBtn.UseVisualStyleBackColor = true;
            this.LockBtn.Click += new System.EventHandler(this.LockBtn_Click);
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(12, 305);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(57, 50);
            this.button3.TabIndex = 3;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Remotebtn_Click);
            // 
            // MonitorBtn
            // 
            this.MonitorBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MonitorBtn.Image = ((System.Drawing.Image)(resources.GetObject("MonitorBtn.Image")));
            this.MonitorBtn.Location = new System.Drawing.Point(12, 240);
            this.MonitorBtn.Name = "MonitorBtn";
            this.MonitorBtn.Size = new System.Drawing.Size(57, 50);
            this.MonitorBtn.TabIndex = 2;
            this.MonitorBtn.UseVisualStyleBackColor = true;
            this.MonitorBtn.Click += new System.EventHandler(this.Monitorbtn_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(12, 170);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(57, 50);
            this.button1.TabIndex = 1;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Homebtn_Click);
            // 
            // Server
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1061, 611);
            this.Controls.Add(this.ShutdownBtn);
            this.Controls.Add(this.RestartBtn);
            this.Controls.Add(this.LockBtn);
            this.Controls.Add(this.FLoating_panel);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.MonitorBtn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Server";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Supervisor Portal";
            this.Load += new System.EventHandler(this.Server_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Table)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button MonitorBtn;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox MsgBox;
        private System.Windows.Forms.TextBox ChatBox;
        private System.Windows.Forms.Panel FLoating_panel;
        private System.Windows.Forms.CheckedListBox Checked_list;
        private System.Windows.Forms.Panel Send_btn;
        private System.Windows.Forms.DataGridView Table;
        private System.Windows.Forms.Button LockBtn;
        private System.Windows.Forms.Button RestartBtn;
        private System.Windows.Forms.Button ShutdownBtn;
        private System.Windows.Forms.Button DownloadBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn StdName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rollno;
        private System.Windows.Forms.DataGridViewTextBoxColumn PORT;
        private System.Windows.Forms.DataGridViewTextBoxColumn IP;
    }
}

