﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    public partial class RemoteDesktop : Form
    {

        public static RemoteDesktop instance;


        public RemoteDesktop()
        {
            InitializeComponent();
            instance = this;
        }

        private void RemoteDesktop_Load(object sender, EventArgs e)
        {
            DisconnectBtn.Enabled = false;
        }

        private void DisconnectBtn_Click(object sender, EventArgs e)
        {
            axMsRdpClient6NotSafeForScripting1.Disconnect();
        }

        private void ConnectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                axMsRdpClient6NotSafeForScripting1.Server = DialogBox.instance.ip.Text;
                axMsRdpClient6NotSafeForScripting1.UserName = DialogBox.instance.user.Text;
                axMsRdpClient6NotSafeForScripting1.AdvancedSettings2.ClearTextPassword = DialogBox.instance.pass.Text;
                axMsRdpClient6NotSafeForScripting1.AdvancedSettings7.EnableCredSspSupport = true;

                axMsRdpClient6NotSafeForScripting1.Connect();
                DisconnectBtn.Enabled = true;
            }
            catch (Exception)
            {
                
                MessageBox.Show("Invalid! Try again", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Hide();
            }
        }
    }
}
