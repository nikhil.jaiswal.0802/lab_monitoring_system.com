﻿namespace Server
{
    partial class Monitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Monitor));
            this.MenuPanel = new System.Windows.Forms.Panel();
            this.menuBtn = new System.Windows.Forms.Panel();
            this.StopRecordBtn = new System.Windows.Forms.Button();
            this.RecordingBtn = new System.Windows.Forms.Button();
            this.CaptureBtn = new System.Windows.Forms.Button();
            this.StopMonitoringBtn = new System.Windows.Forms.Button();
            this.MonitorBtn = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Menu_timer = new System.Windows.Forms.Timer(this.components);
            this.Screen = new System.Windows.Forms.PictureBox();
            this.Recording_timer = new System.Windows.Forms.Timer(this.components);
            this.MenuPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Screen)).BeginInit();
            this.SuspendLayout();
            // 
            // MenuPanel
            // 
            this.MenuPanel.BackColor = System.Drawing.Color.Black;
            this.MenuPanel.Controls.Add(this.menuBtn);
            this.MenuPanel.Controls.Add(this.StopRecordBtn);
            this.MenuPanel.Controls.Add(this.RecordingBtn);
            this.MenuPanel.Controls.Add(this.CaptureBtn);
            this.MenuPanel.Controls.Add(this.StopMonitoringBtn);
            this.MenuPanel.Controls.Add(this.MonitorBtn);
            this.MenuPanel.Controls.Add(this.panel2);
            this.MenuPanel.Location = new System.Drawing.Point(2, 0);
            this.MenuPanel.MaximumSize = new System.Drawing.Size(230, 798);
            this.MenuPanel.MinimumSize = new System.Drawing.Size(62, 798);
            this.MenuPanel.Name = "MenuPanel";
            this.MenuPanel.Size = new System.Drawing.Size(62, 798);
            this.MenuPanel.TabIndex = 0;
            // 
            // menuBtn
            // 
            this.menuBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("menuBtn.BackgroundImage")));
            this.menuBtn.Location = new System.Drawing.Point(14, 27);
            this.menuBtn.Name = "menuBtn";
            this.menuBtn.Size = new System.Drawing.Size(31, 30);
            this.menuBtn.TabIndex = 7;
            this.menuBtn.Click += new System.EventHandler(this.MenuBtn_Click);
            // 
            // StopRecordBtn
            // 
            this.StopRecordBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.StopRecordBtn.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StopRecordBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(188)))), ((int)(((byte)(215)))));
            this.StopRecordBtn.Image = ((System.Drawing.Image)(resources.GetObject("StopRecordBtn.Image")));
            this.StopRecordBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.StopRecordBtn.Location = new System.Drawing.Point(3, 450);
            this.StopRecordBtn.Name = "StopRecordBtn";
            this.StopRecordBtn.Size = new System.Drawing.Size(213, 65);
            this.StopRecordBtn.TabIndex = 6;
            this.StopRecordBtn.Text = "       Stop recording";
            this.StopRecordBtn.UseVisualStyleBackColor = true;
            this.StopRecordBtn.Click += new System.EventHandler(this.StopRecordBtn_Click);
            // 
            // RecordingBtn
            // 
            this.RecordingBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.RecordingBtn.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RecordingBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(188)))), ((int)(((byte)(215)))));
            this.RecordingBtn.Image = ((System.Drawing.Image)(resources.GetObject("RecordingBtn.Image")));
            this.RecordingBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.RecordingBtn.Location = new System.Drawing.Point(3, 379);
            this.RecordingBtn.Name = "RecordingBtn";
            this.RecordingBtn.Size = new System.Drawing.Size(213, 65);
            this.RecordingBtn.TabIndex = 5;
            this.RecordingBtn.Text = "         Start Recording";
            this.RecordingBtn.UseVisualStyleBackColor = true;
            this.RecordingBtn.Click += new System.EventHandler(this.RecordingBtn_Click);
            // 
            // CaptureBtn
            // 
            this.CaptureBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CaptureBtn.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CaptureBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(188)))), ((int)(((byte)(215)))));
            this.CaptureBtn.Image = ((System.Drawing.Image)(resources.GetObject("CaptureBtn.Image")));
            this.CaptureBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CaptureBtn.Location = new System.Drawing.Point(3, 308);
            this.CaptureBtn.Name = "CaptureBtn";
            this.CaptureBtn.Size = new System.Drawing.Size(213, 65);
            this.CaptureBtn.TabIndex = 4;
            this.CaptureBtn.Text = "Capture";
            this.CaptureBtn.UseVisualStyleBackColor = true;
            this.CaptureBtn.Click += new System.EventHandler(this.CaptureBtn_Click);
            // 
            // StopMonitoringBtn
            // 
            this.StopMonitoringBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.StopMonitoringBtn.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StopMonitoringBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(188)))), ((int)(((byte)(215)))));
            this.StopMonitoringBtn.Image = ((System.Drawing.Image)(resources.GetObject("StopMonitoringBtn.Image")));
            this.StopMonitoringBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.StopMonitoringBtn.Location = new System.Drawing.Point(3, 237);
            this.StopMonitoringBtn.Name = "StopMonitoringBtn";
            this.StopMonitoringBtn.Size = new System.Drawing.Size(213, 65);
            this.StopMonitoringBtn.TabIndex = 3;
            this.StopMonitoringBtn.Text = "Stop";
            this.StopMonitoringBtn.UseVisualStyleBackColor = true;
            this.StopMonitoringBtn.Click += new System.EventHandler(this.StopMonitoringBtn_Click);
            // 
            // MonitorBtn
            // 
            this.MonitorBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.MonitorBtn.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MonitorBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(188)))), ((int)(((byte)(215)))));
            this.MonitorBtn.Image = ((System.Drawing.Image)(resources.GetObject("MonitorBtn.Image")));
            this.MonitorBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MonitorBtn.Location = new System.Drawing.Point(3, 166);
            this.MonitorBtn.Name = "MonitorBtn";
            this.MonitorBtn.Size = new System.Drawing.Size(213, 65);
            this.MonitorBtn.TabIndex = 2;
            this.MonitorBtn.Text = "Monitor";
            this.MonitorBtn.UseVisualStyleBackColor = true;
            this.MonitorBtn.Click += new System.EventHandler(this.MonitorBtn_Click);
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(229, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1007, 709);
            this.panel2.TabIndex = 1;
            // 
            // Menu_timer
            // 
            this.Menu_timer.Tick += new System.EventHandler(this.Menu_timer_tick);
            // 
            // Screen
            // 
            this.Screen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.Screen.Location = new System.Drawing.Point(238, 26);
            this.Screen.Name = "Screen";
            this.Screen.Size = new System.Drawing.Size(1149, 756);
            this.Screen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Screen.TabIndex = 1;
            this.Screen.TabStop = false;
            // 
            // Recording_timer
            // 
            this.Recording_timer.Tick += new System.EventHandler(this.Recording_timer_tick);
            // 
            // Monitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(24)))), ((int)(((byte)(24)))));
            this.ClientSize = new System.Drawing.Size(1408, 794);
            this.Controls.Add(this.Screen);
            this.Controls.Add(this.MenuPanel);
            this.ForeColor = System.Drawing.Color.Chocolate;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Monitor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Monitor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Monitor_Close);
            this.Load += new System.EventHandler(this.Monitor_Load);
            this.MenuPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Screen)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel MenuPanel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button MonitorBtn;
        private System.Windows.Forms.PictureBox Screen;
        private System.Windows.Forms.Button StopRecordBtn;
        private System.Windows.Forms.Button RecordingBtn;
        private System.Windows.Forms.Button CaptureBtn;
        private System.Windows.Forms.Button StopMonitoringBtn;
        private System.Windows.Forms.Panel menuBtn;
        private System.Windows.Forms.Timer Menu_timer;
        private System.Windows.Forms.Timer Recording_timer;
    }
}